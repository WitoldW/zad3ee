<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.witoldw.zad3.Domain.*" %>
<%@page import="com.witoldw.zad3.Service.*" %>
<%@page import="com.witoldw.zad3.*" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tabela</title>
    </head>
    <body>
        <jsp:useBean id="basicInformation" class="com.witoldw.zad3.Domain.BasicInformation" scope="session"/>
        <jsp:useBean id="salaryCalc" class="com.witoldw.zad3.Domain.SalaryCalculatorApp" scope="session"/>
        <jsp:useBean id="calculatorService" class="com.witoldw.zad3.Service.SalaryCalculatorService" scope="application"/>
        <jsp:useBean id="specificTask" class="com.witoldw.zad3.Domain.SpecificTaskContract" scope="session"/>
        <jsp:useBean id="feeTask" class="com.witoldw.zad3.Domain.FeeForTaskAgreement" scope="session"/>
        <c:choose>
            <c:when test="${basicInformation.genreOfContracts eq 'specifictask'}" >
                <jsp:setProperty name="specificTask" property="costOfIncome" param="costofincome" />
            </c:when>
            <c:when test="${basicInformation.genreOfContracts eq 'feefortask'}" >
                <jsp:setProperty name="feeTask" property="costOfIncome" param="costofincome" />
                <jsp:setProperty name="feeTask" property="duesOfDisease" param="duesofdisease" />
                <jsp:setProperty name="feeTask" property="duesOfHealth" param="duesofhealth" />
                <jsp:setProperty name="feeTask" property="duesOfPension" param="duesofpension" />
                <jsp:setProperty name="feeTask" property="duesOfRetirement" param="duesofretirement" />
            </c:when>
        </c:choose>
        <% 
            salaryCalc.setBasicInformation(basicInformation);
            salaryCalc.setFeeForTaskAgreement(feeTask);
            salaryCalc.setSpecificTaskContract(specificTask);
            calculatorService.setSalaryCalculatorApp(salaryCalc);
        %>

      
        ${calculatorService.generate()}

    </body>
</html>
