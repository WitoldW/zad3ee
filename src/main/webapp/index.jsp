<%@page import="com.witoldw.zad3.Domain.SalaryCalculatorApp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Kalkulator płac</title>
    </head>
    <body>
        
        <h1>Kalkulator płac</h1>

        <form action="next.jsp" method="get">
            Rodzaj umowy:<br/>
            <input type="radio" name="contract" value="job" checked> Umowa o prace<br>
            <input type="radio" name="contract" value="specifictask"> Umowa o dzieło<br>
            <input type="radio" name="contract" value="feefortask"> Umowa zlecenie<br/>
            <label for="year">Rok</label>  <input type="number" id="year" name="year" value="2016"/><br/>
            Kwota:<br/>
            <input type="radio" name="genreofsalary" value="brutto" checked> Brutto<br>
            <input type="radio" name="genreofsalary" value="netto"> Netto<br>
            <label for="amountofsalary">Kwota wynagrodzenia: </label> <input type="number" id="amountofsalary" name="amountofsalary">
            <input type="submit" value="Dalej..."/>
        </form>
    </body>
</html>
