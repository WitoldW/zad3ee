<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.witoldw.zad3.Domain.BasicInformation" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Kalkulator płac</title>
    </head>
    <body>
        <jsp:useBean id="basicInformation" class="com.witoldw.zad3.Domain.BasicInformation" scope="session"/>
        <jsp:setProperty name="basicInformation" property="year"  param="year" /> 
        <jsp:setProperty name="basicInformation" property="genreOfContracts"  param="contract" />
        <jsp:setProperty name="basicInformation" property="genreOfSalary"  param="genreofsalary" />        
        <jsp:setProperty name="basicInformation" property="amountOfSalary"  param="amountofsalary" />  

        <h1>Kalkulator płac</h1>
        <!-- 1 opcja -->
        <c:if test="${basicInformation.genreOfContracts eq 'job'}" >
            <c:redirect url="generate.jsp"/>
        </c:if>
        <!-- 2 opcja -->
        <c:if test="${basicInformation.genreOfContracts eq 'feefortask'}" >

            <form id="option2" action="generate.jsp" method="post">
                Składka rentowa:<br/>
                <input type="radio" name="duesofpension" value="true" checked> tak<br>
                <input type="radio" name="duesofpension" value="false"> nie<br>
                Składka emerytalna:<br/>
                <input type="radio" name="duesofretirement" value="true" checked> tak<br>
                <input type="radio" name="duesofretirement" value="false"> nie<br>
                Składka chorobowa:<br/>
                <input type="radio" name="duesofdisease" value="true" checked> tak<br>
                <input type="radio" name="duesofdisease" value="false"> nie<br>
                Składka zdrowotna: <br/>
                <input type="radio" name="duesofhealth" value="true" checked> tak<br>
                <input type="radio" name="duesofhealth" value="false"> nie<br>
                Koszt uzyskania przychodu:<br/>
                <input type="radio" name="costofincome" value="20" checked> 20%<br>
                <input type="radio" name="costofincome" value="50"> 50%<br>
                <input type="submit" value="Generuj" />
            </form>
        </c:if>
        <!-- ################# -->

        <!-- 3 opcja -->
        <c:if test="${basicInformation.genreOfContracts eq 'specifictask'}" > 
            <form action="generate.jsp" method="post">
                Koszt uzyskania przychodu:<br/>
                <input type="radio" name="costofincome" value="20" checked> 20%<br>
                <input type="radio" name="costofincome" value="50"> 50%<br>
                <input type="submit" value="Generuj" />
            </form>
        </c:if>
    </body>
</html>
