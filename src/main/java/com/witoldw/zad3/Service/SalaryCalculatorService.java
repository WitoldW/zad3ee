package com.witoldw.zad3.Service;

import com.witoldw.zad3.Domain.BasicInformation;
import com.witoldw.zad3.Domain.FeeForTaskAgreement;
import com.witoldw.zad3.Domain.SalaryCalculatorApp;
import com.witoldw.zad3.Domain.SpecificTaskContract;

public class SalaryCalculatorService {

    private SalaryCalculatorApp salaryCalculatorApp;

    public SalaryCalculatorApp getSalaryCalculatorApp() {
        return salaryCalculatorApp;
    }

    public void setSalaryCalculatorApp(SalaryCalculatorApp salaryCalculatorApp) {
        this.salaryCalculatorApp = salaryCalculatorApp;
    }

    public String generate() {
        switch (salaryCalculatorApp.getBasicInformation().getGenreOfContracts()) {
            case "feefortask":
                return generateFeeForTask();
            case "specifictask":
                return generateSpecificTask();
            default:
                return generateJob();
        }
    }

    private String generateFeeForTask() {
        BasicInformation bi = salaryCalculatorApp.getBasicInformation();
        FeeForTaskAgreement stc = salaryCalculatorApp.getFeeForTaskAgreement();
        double cost = bi.getAmountOfSalary() * (stc.getCostOfIncome() / 100.0);
        double ret = 0.0;
        double health = 0.0;
        double sick = 0.0;
        double pension = 0.0;

        if (stc.isDuesOfRetirement()) {
            ret = Math.round( (bi.getAmountOfSalary() * (9.76 / 100.0)) *100)/100;
        }
        if (stc.isDuesOfDisease()) {
            sick = Math.round((bi.getAmountOfSalary() * (2.45 / 100.0))*100)/100;
        }
        if (stc.isDuesOfHealth()) {
            health = Math.round((bi.getAmountOfSalary() * (9 / 100.0))*100)/100;
        }
        if (stc.isDuesOfPension()) {
            pension = Math.round((bi.getAmountOfSalary() * (1.5 / 100.0))*100)/100;
        }

        double pit = Math.round(((bi.getAmountOfSalary() - cost - ret - health - sick - pension) * (18 / 100.0)) * 100.0) / 100.0;
        double netto = Math.round((bi.getAmountOfSalary() - pit - ret - health - sick - pension) * 100.0) / 100.0;
        String table = "<table border=\"1\" cellpadding=\"5\" cellspacing=\"5\" style=\"text-align:center;\" ><tr><td rowspan=\"2\" >Brutto</td>"
                + "<td colspan=\"4\">Ubezpieczenie</td><td rowspan=\"2\" >Koszt uzyskania przychodu</td><td rowspan=\"2\" >Podstawa opodatkowania</td>"
                + "<td rowspan=\"2\" >Zaliczka na PIT</td><td rowspan=\"2\" >Netto</td></tr> ";
        table += "<td>Emerytalne</td><td>Rentowe</td><td>Chorobowe</td><td>Zdrowotne</td>"
                + "<tr><td>" + bi.getAmountOfSalary() + "</td><td>" + ret + "</td>" + "<td>" + pension + "</td>" + "<td>" + sick + "</td>" + "<td>" + health + "</td>"
                + "<td>" + cost + "</td><td>" + (bi.getAmountOfSalary() - cost - ret - health - sick - pension) + "</td><td>" + pit + "</td><td>" + netto + "</td></tr></table>";

        return table;
    }

    private String generateSpecificTask() {
        BasicInformation bi = salaryCalculatorApp.getBasicInformation();
        SpecificTaskContract stc = salaryCalculatorApp.getSpecificTaskContract();
        double cost = bi.getAmountOfSalary() * (stc.getCostOfIncome() / 100.0);
        double pit = (bi.getAmountOfSalary() - cost) * (18 / 100.0);
        double netto = bi.getAmountOfSalary() - pit;
        String table = "<table border=\"1\" cellpadding=\"5\" cellspacing=\"5\" style=\"text-align:center;\" ><tr><td>Brutto</td>"
                + "<td>Koszt uzyskania przychodu</td><td>Podstawa opodatkowania</td><td>Zaliczka na PIT</td><td>Netto</td></tr> ";
        table += "<tr><td>" + bi.getAmountOfSalary() + "</td><td>" + cost + "</td><td>" + (bi.getAmountOfSalary() - cost) + "</td><td>" + pit + "</td><td>" + netto + "</td></tr></table>";
        return table;
    }

    private String generateJob() {
        BasicInformation bi = salaryCalculatorApp.getBasicInformation();
        String months[] = {"Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"};
        double ret = Math.round((bi.getAmountOfSalary() * (9.76 / 100.0))*100)/100;
        double sick = Math.round((bi.getAmountOfSalary() * (2.45 / 100.0))*100)/100;
        double pension = Math.round((bi.getAmountOfSalary() * (1.5 / 100.0))*100)/100;
        double zaliczka = Math.round((bi.getAmountOfSalary() - ret - sick - pension)*100)/100;
        double health = Math.round((zaliczka * (9 / 100.0))*100)/100;
        double zaliczkaNaPit = Math.round(((zaliczka - 111.25) * (18 / 100.0) - 46.33 - (zaliczka * (7.75 / 100.0)))*100)/100;
        double netto = Math.round((bi.getAmountOfSalary() - ret - health - sick - pension - zaliczkaNaPit) * 100.0) / 100.0;
        String table = "<table border=\"1\" cellpadding=\"5\" cellspacing=\"5\" style=\"text-align:center;\" ><tr><td rowspan=\"2\" ></td><td rowspan=\"2\" >Brutto</td>"
                + "<td colspan=\"4\">Ubezpieczenie</td><td rowspan=\"2\" >Podstawa opodatkowania</td>"
                + "<td rowspan=\"2\" >Zaliczka na PIT</td><td rowspan=\"2\" >Netto</td></tr> ";
        table += "<td>Emerytalne</td><td>Rentowe</td><td>Chorobowe</td><td>Zdrowotne</td>";
        for (int i = 0; i < 12; i++) {
            table += "<tr><td>" + months[i] + "</td><td>" + bi.getAmountOfSalary() + "</td><td>" + ret + "</td><td>" + pension + "</td><td>" + sick + "</td><td>" + health + "</td>"
                    + "<td>" + (bi.getAmountOfSalary() - ret - health - sick - pension) + "</td><td>" + zaliczkaNaPit + "</td><td>" + netto + "</td></tr>";
        }
        table += "<tr><td>Suma</td><td>" + 12 * bi.getAmountOfSalary() + "</td><td>" + Math.round(12.0 * ret * 100.0) / 100.0 + "</td><td>" + 12 * pension + "</td><td>" + 12 * sick + "</td><td>" + 12 * health + "</td><td>" + 12 * zaliczka + "</td><td>" + Math.round(12 * zaliczkaNaPit * 100.0) / 100.0 + "</td><td>" + 12 * netto + "</td></tr></table>";
        return table;
    }
}
