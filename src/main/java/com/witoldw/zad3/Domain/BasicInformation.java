package com.witoldw.zad3.Domain;

import java.util.Objects;

public class BasicInformation {
    
    private String genreOfContracts;
    private int year;
    private String genreOfSalary;
    private double amountOfSalary;

   
    public String getGenreOfContracts() {
        return genreOfContracts;
    }

    public void setGenreOfContracts(String genreOfContracts) {
        this.genreOfContracts = genreOfContracts;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getGenreOfSalary() {
        return genreOfSalary;
    }

    public void setGenreOfSalary(String genreOfSalary) {
        this.genreOfSalary = genreOfSalary;
    }

    public double getAmountOfSalary() {
        return amountOfSalary;
    }

    public void setAmountOfSalary(double amountOfSalary) {
        this.amountOfSalary = amountOfSalary;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.genreOfContracts);
        hash = 59 * hash + this.year;
        hash = 59 * hash + Objects.hashCode(this.genreOfSalary);
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.amountOfSalary) ^ (Double.doubleToLongBits(this.amountOfSalary) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BasicInformation other = (BasicInformation) obj;
        if (this.year != other.year) {
            return false;
        }
        if (Double.doubleToLongBits(this.amountOfSalary) != Double.doubleToLongBits(other.amountOfSalary)) {
            return false;
        }
        if (!Objects.equals(this.genreOfContracts, other.genreOfContracts)) {
            return false;
        }       
        return true;
    }

    
 
    
    
}
