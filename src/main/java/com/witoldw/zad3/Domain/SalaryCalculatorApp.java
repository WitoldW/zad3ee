package com.witoldw.zad3.Domain;


public class SalaryCalculatorApp {

    private BasicInformation basicInformation;
    private FeeForTaskAgreement feeForTaskAgreement;
    private SpecificTaskContract specificTaskContract;

    public BasicInformation getBasicInformation() {
        return basicInformation;
    }

    public void setBasicInformation(BasicInformation basicInformation) {
        this.basicInformation = basicInformation;
    }

    public FeeForTaskAgreement getFeeForTaskAgreement() {
        return feeForTaskAgreement;
    }

    public void setFeeForTaskAgreement(FeeForTaskAgreement feeForTaskAgreement) {
        this.feeForTaskAgreement = feeForTaskAgreement;
    }

    public SpecificTaskContract getSpecificTaskContract() {
        return specificTaskContract;
    }

    public void setSpecificTaskContract(SpecificTaskContract specificTaskContract) {
        this.specificTaskContract = specificTaskContract;
    }
    
    
        
}
