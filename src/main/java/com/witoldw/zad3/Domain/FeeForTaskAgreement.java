package com.witoldw.zad3.Domain;

import java.util.Objects;


public class FeeForTaskAgreement {
    
    private boolean duesOfPension;  //renta
    private boolean duesOfRetirement;  
    private boolean duesOfDisease; // 
    private boolean duesOfHealth;
    private int costOfIncome;

   
    public boolean isDuesOfPension() {
        return duesOfPension;
    }

    public void setDuesOfPension(boolean duesOfPension) {
        this.duesOfPension = duesOfPension;
    }

    public boolean isDuesOfRetirement() {
        return duesOfRetirement;
    }

    public void setDuesOfRetirement(boolean duesOfRetirement) {
        this.duesOfRetirement = duesOfRetirement;
    }

    public boolean isDuesOfDisease() {
        return duesOfDisease;
    }

    public void setDuesOfDisease(boolean duesOfDisease) {
        this.duesOfDisease = duesOfDisease;
    }

    public boolean isDuesOfHealth() {
        return duesOfHealth;
    }

    public void setDuesOfHealth(boolean duesOfHealth) {
        this.duesOfHealth = duesOfHealth;
    }

    public int getCostOfIncome() {
        return costOfIncome;
    }

    public void setCostOfIncome(int costOfIncome) {
        this.costOfIncome = costOfIncome;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.duesOfPension ? 1 : 0);
        hash = 17 * hash + (this.duesOfRetirement ? 1 : 0);
        hash = 17 * hash + (this.duesOfDisease ? 1 : 0);
        hash = 17 * hash + (this.duesOfHealth ? 1 : 0);
        hash = 17 * hash + Objects.hashCode(this.costOfIncome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FeeForTaskAgreement other = (FeeForTaskAgreement) obj;
        if (this.duesOfPension != other.duesOfPension) {
            return false;
        }
        if (this.duesOfRetirement != other.duesOfRetirement) {
            return false;
        }
        if (this.duesOfDisease != other.duesOfDisease) {
            return false;
        }
        if (this.duesOfHealth != other.duesOfHealth) {
            return false;
        }
        if (this.costOfIncome != other.costOfIncome) {
            return false;
        }
        return true;
    }
    
    
    
    

}
