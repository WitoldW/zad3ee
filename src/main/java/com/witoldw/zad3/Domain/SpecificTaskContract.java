package com.witoldw.zad3.Domain;

import java.util.Objects;


public class SpecificTaskContract {

    private int costOfIncome;

    public int getCostOfIncome() {
        return costOfIncome;
    }

    public void setCostOfIncome(int costOfIncome) {
        this.costOfIncome = costOfIncome;
    }

    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.costOfIncome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SpecificTaskContract other = (SpecificTaskContract) obj;
        if (this.costOfIncome != other.costOfIncome) {
            return false;
        }
        return true;
    }
    
    
}
